'use strict';

module.exports = {
	'extends': [
		'eslint:recommended',
		'plugin:react/recommended'
	],
	"plugins": [
		"react"
	],
	"globals": {
		"console": true
	},
	'parserOptions': {
		"ecmaVersion": 8,
		"ecmaFeatures": {
			"jsx": true
		}
	},
	'env': {
		'node': true,
		'browser': true,
		"es6": true,
		"mocha": true
	},
	'rules': {
		// enable additional rules
		'no-template-curly-in-string': 'error',
		'valid-jsdoc': 'error',
		'block-scoped-var': 'error',
		'default-case': 'error',
		'eqeqeq': ['error', 'smart'],
		'no-else-return': 'error',
		'no-eval': 'error',
		'vars-on-top': 'error',
		'strict': [2, 'global'],
		'no-shadow': 'error',
		'no-use-before-define': 'error',
		'brace-style': ['error', '1tbs'],
		'comma-spacing': ['error', {'before': false, 'after': true}],
		'comma-style': ['error', 'last'],
		'block-spacing': ['error', 'always'],
		'func-call-spacing': ['error', 'never'],
		'indent': ['error', 'tab'],
		'keyword-spacing': ['error', {'before': true, 'after': true}],
		'max-len': ['error', {'tabWidth': 1, 'code': 140}],
		'no-bitwise': 'error',
		'no-mixed-spaces-and-tabs': 'error',
		'no-trailing-spaces': 'error',
		'arrow-body-style': ['error', 'always'],
		'arrow-parens': ['error', 'as-needed'],
		'arrow-spacing': ['error', {'before': true, 'after': true}],
		'no-nested-ternary': 'error',
		'no-unneeded-ternary': 'error',
		'quotes': ['error', 'single', { 'allowTemplateLiterals': true }],
		"comma-dangle": [2, "never"], 
		"no-cond-assign": [2, "except-parens"],
		"no-console": 1,
		"no-debugger": 1,
		"no-alert": 1,
		"no-constant-condition": 1,
		"no-dupe-keys": 2,
		"no-duplicate-case": 2,
		"no-empty": 2,
		"no-ex-assign": 2,
		"no-extra-boolean-cast": 0,
		"no-extra-semi": 2,
		"no-func-assign": 2,
		"no-inner-declarations": 2,
		"no-invalid-regexp": 2,
		"no-irregular-whitespace": 2,
		"no-obj-calls": 2,
		"no-sparse-arrays": 2,
		"no-unreachable": 2,
		"use-isnan": 2,
		"block-scoped-var": 2
	}
}