
# Company Domain Search

Enter up to 25 company names to receive the best possible domain match for those companies.

## App

The App can be accessed and used at the following address for review.

> [http://35.237.229.80/](http://35.237.229.80/)

## Postgres

Database can be connected too and reviewed with the credentials below.

> * host: '35.237.229.80'
> * user: 'postgres'
> * database: 'postgres'
> * password: 'seamlessAI'
> * port: 5432

## Authorization

> JWT token used as an access token for `/url` requests (`Authorization: Bearer ....`). This token lives for 5 minutes. Refreshing will generate a new token. 
> If token is valid user is then verified in the database as an active user. The User Hash is `bcrypt.compare` to verify the proper user to token relationship. Then checks the active value in the DB.

## Development

```
// App (root)
npm run dev

// Client (/client)
npm start
```

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).