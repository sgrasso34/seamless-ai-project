'use strict'

const db = require('../db');
const bcrypt = require('bcrypt');

module.exports = {

	find: name => {
		return new Promise((resolve, reject) => {

			db.findUserByName([name], (err, res) => {

				if (err) {
					console.error(err);
					return reject({status: 500, message: 'Danger Will Robinson! Danger!'});
				}

				// No Results No User Found
				if (!res || !res.rows.length) return reject({status: 403, message: 'Forbidden'});

				// Not an Active API User
				if (!res.rows[0].active) return reject({status: 401, message: 'Permission Denied'});

				return resolve(res.rows[0]);
			});
		});
	},

	checkHash: (reqPassword, userPassword) => {
		return new Promise((resolve, reject) => {

			bcrypt.compare(reqPassword, userPassword, (err, response) => {
				if (err) {
					return reject({status: 500, message: err});
				} else if (response) {
					return resolve(response);
				}
				return reject({status: 401, message: new Error('Access Denied')});
			});
		});
	}
}