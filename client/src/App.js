import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import 'font-awesome/css/font-awesome.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
import './App.css';

import Home from './views/Home';

export default class App extends Component {

	render() {
		const App = () => (
			<div>
				<Switch>
					<Route exact path='/' component={Home}/>
				</Switch>
			</div>
		)
		return (
			<Switch>
				<App/>
			</Switch>
		);
	}
}