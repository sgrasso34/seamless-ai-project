import React, { Component } from 'react';
import { Table } from 'mdbreact';
export default class Results extends Component {

	render() {
		const { error, show, isLoading, domains } = this.props.state;

		if (!show) {
			return '';
		}

		if (error) {
			return (<p className="my-4">{error.message}</p>);
		}

		if (isLoading) {
			return (<p className="my-4">Loading ...</p>);
		}

		if (!domains.length) 
			return (<h4 className="my-4">No results</h4>);

		return (
			<Table striped responsive bordered className="comapany-row companyFlip">
				<tbody>
					{domains.map((d, index) =>
						<tr key={index}>
							<th scope="row">{++index}</th>
							<td className="domainName">{d.name}</td>
							<td><a href={'//' + d.domain}>{d.domain}</a></td>
						</tr>
					)}
				</tbody>
			</Table>
		);
	}
}
