import React, { Component } from 'react';
import axios from 'axios';
import { Container, Row, Col, Card, CardBody, Button } from 'mdbreact';
import TagsInput from 'react-tagsinput'
import 'react-tagsinput/react-tagsinput.css'

import './home.css'
import mainLogo from '../../resources/logo_sticky.png';
import Results from '../../components/Results';

export default class Home extends Component {

	componentDidMount() {
		
		// Login and Gather User token on first mount... Typically done on a login page
		axios.post('/token', {
			username: 'seamless1',
			password: 'seamless1Psw'
		})
		.then(resp => {
			this.setState({
				token: resp.data.token
			});
		})
		.catch(error => {
			throw error;
		});
	}

	constructor(props) {

		super(props);

		this.state = {
			companies: [],
			domains: [],
			isLoading: false,
			show: false,
			error: null,
			token: ''
		}

		this.getUrls = this.getUrls.bind(this);
		this.handleChange = this.handleChange.bind(this);
	}

	handleChange(companies) {
		this.setState({ companies });
	}

	getUrls() {
		const companies = this.state.companies;
		const companiesLen = companies.length;
		let reqArray = [], respDomains = [];

		this.setState({
			isLoading: true
		});

		// Build array of requests to execute in parallel
		for (let i = 0; i < companiesLen; i++) {
			reqArray.push(axios.get('/url',
				{ 
					params: {
						companies: companies[i]
					},
					headers: {
						authorization: this.state.token
					}
				})
			);
		}

		axios.all(reqArray)
			.then(response => {
				for (let ii = 0; ii < response.length; ii++) {
					respDomains.push(response[ii].data);
				}

				this.setState({
					domains: respDomains,
					show: true,
					isLoading: false
				});
			})
			.catch(error => {
				this.setState({
					error,
					domains:[],
					show: true,
					isLoading: false
				});

				throw error;
			}
		);
	}

	render() {
		return(
			<Container>
				<section className="form-elegant">
					<Row>
						<Col md="6">
							<Card>
								<CardBody className="mx-3">
									<div className="text-center">
										<div className="logospot"><img  src={mainLogo} alt="seamless.ai"/></div>
									</div>
									<div className="text-center mb-2">
										<TagsInput value={this.state.companies} onChange={this.handleChange} maxTags="25" addOnBlur addOnPaste onlyUnique inputProps={{placeholder: "Add a Company"}}/>
										<Button type="button" onClick={this.getUrls} rounded className="btn-block z-depth-1a">Search</Button>
									</div>
									<div className="d-flex justify-content-center resultsContainer">
										<Results state={this.state}/>
									</div>
								</CardBody>
							</Card>
						</Col>
					</Row>
				</section>
			</Container>
		);
	}
}
