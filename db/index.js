'use strict'

// CREATE TABLE public.events
// (
// time_stamp timestamp(6) without time zone NOT NULL DEFAULT now(),
// query text COLLATE pg_catalog."default",
// duration character varying(255) COLLATE pg_catalog."default",
// total_results character varying(255) COLLATE pg_catalog."default",
// id integer NOT NULL DEFAULT nextval('events_id_seq'::regclass),
// CONSTRAINT id PRIMARY KEY (id)
// )

const { Pool } = require('pg');

// Should be set in environment variables
const pool = new Pool({
	host: process.env.DB_HOST,
	user: process.env.DB_USER,
	database: process.env.DB_DATABASE,
	password: process.env.DB_PASSWORD,
	port: process.env.DB_PORT
});

pool.on('error', err => {
	console.error('Unexpected error on idle client', err);
	process.exit(-1);
});

module.exports = {
	insertDuration: (params, next) => {
		return pool.query('INSERT INTO public.events(query, duration, total_results) VALUES($1, $2, $3)',
			params,
			(err, res) => {
				console.log('executed query - INSERT Event Duration', {rows: res.rowCount});
				return next(err, res);
			}
		);
	},
	findUserByName: (params, next) => {
		return pool.query('SELECT username, hash, active FROM public.users WHERE username = $1',
			params,
			next
		);
	}
}