'use strict'

const express = require('express');
const path = require('path');
const port = process.env.PORT || 5000;
const app = express();
const routes = require('./routes');

// Serve the static files from the React app
app.use(express.static(path.join(__dirname, 'client/build')));

// Handles any requests made; sends to router
routes(app);

app.listen(port);

console.log('App is listening on port ' + port);