'use strict'

const path = require('path');
const url = require('./url');
const token = require('./token');
const jsonParser = require('body-parser').json();

module.exports = app => {

	app.use('/url', url);
	app.use('/token', jsonParser, token);

	app.use('/', (req, res) => {
		if (process.env.NODE_ENV === 'production')
			return res.sendFile(path.join(__dirname, '..', 'client', 'build', 'index.html'));
		return res.sendFile(path.join(__dirname + '../client/public/index.html'));
	});

	app.use('*', (req, res) => {
		res.status(404).end();
	});
}