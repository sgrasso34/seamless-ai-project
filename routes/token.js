'use strict'

// CREATE TABLE public.users
// (
// 	id integer NOT NULL DEFAULT nextval('users_id_seq'::regclass),
// 	username text COLLATE pg_catalog."default" NOT NULL,
// 	hash text COLLATE pg_catalog."default" NOT NULL,
// 	CONSTRAINT users_pkey PRIMARY KEY (id),
// 	CONSTRAINT username UNIQUE (username)
// )

const asyncRoute = require('route-async');
const jwt = require('jsonwebtoken');
const User = require('../auth/user');

const tokenRoute = async (req, res) => {

	let token = '', user = {};
	let { username, password } = req.body;

	if (!username || !password) {

		// Missing credentials
		return res.status(422).send({
			auth: false,
			message: 'Must provide credentials to proceed'
		});
	}

	try {
		user = await User.find(username);
		await User.checkHash(`${username}:${password}`, user.hash);
	} catch (e) {
		// User not found or User not active or Error occurred.
		return res.status(e.status).send({
			auth: false,
			message: e.message
		});
	}

	token = jwt.sign(
		{
			name: user.username,
			hash: user.hash
		},
		process.env.TOKEN_SECRET,
		{expiresIn: 300} // expires in 5 minutes for testing
	);

	return res.send({
		auth: true,
		token: token
	});
};

module.exports = asyncRoute(tokenRoute);
