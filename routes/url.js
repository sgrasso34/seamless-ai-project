'use strict'

const asyncRoute = require('route-async');
const axios = require('axios');
const jwt = require('jsonwebtoken');
const db = require('../db');
const config = require('../config.json');
const User = require('../auth/user');

const urlRoute = async (req, res) => {

	const token = req.headers['authorization'];
	const params = Object.assign(
		{},
		{q: 'inurl:' + req.query.companies},
		{cx: config.cx, key: config.key}
	);

	// Request Missing Auth Token.
	if (!token) return res.status(401).send({
		auth: false,
		message: 'Permission Denied'
	});

	jwt.verify(token, process.env.TOKEN_SECRET, async (err, decoded) => {
		let resp = {};

		// Token Authentication Failed.
		if (err) return res.status(401).send({
			auth: false,
			message: 'Permission Denied'
		});

		try {
			// Retrieve User from DB.
			await User.find(decoded.name);

			// Call Google CSE
			resp = await axios.get(config.url, {
				params: params
			});

			// Record Query Durations
			db.insertDuration([
				req.query.companies,
				resp.data.searchInformation.formattedSearchTime,
				resp.data.searchInformation.formattedTotalResults
			], e => {
				if (e) console.error(e);
			});

			// Assuming the first item is most accurate since it is the most likely to have the company name in the URL
			return res.send({
				name: resp.data.items[0].htmlTitle,
				domain: resp.data.items[0].displayLink
			});

		} catch (error) {
			console.error(error);

			// Error occurred
			return res.status(error.status).send({
				auth: false,
				message: error.message
			});
		}
	});
};

module.exports = asyncRoute(urlRoute);
