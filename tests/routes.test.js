'use strict'

const sinon = require('sinon');
// const User = require('../auth/user');
const { expect } = require('chai');
const tokenHandler = require('../routes/token.js');

describe('express routes', function () {

	const req = {
		body: {
			password: 'seamless1',
			username: 'seamless1Psw'
		}
	};
	const res = {
		status: () => {
			return true;
		},
		send: sinon.stub()
	};

	const User = {
		find: () => {
			return {};
		},
		checkHash: () => {
			return {};
		}
	}

	const resetStubs = () => {
		res.send.resetHistory();
	}

	// context('no errors', () => {

	// 	before(async () => {
	// 		await tokenHandler(req, res);
	// 	});

	// 	after(() => {
	// 		resetStubs();
	// 	});

	// 	it('called someAsync with the right data', () => {
	// 		expect(res.send.called).to.be.true;

	// 	});
	// });
});