'use strict'

const sinon = require('sinon');
const db = require('../db');
const User = require('../auth/user');
const { expect } = require('chai');

describe('User logic', function () {

	context('no errors', () => {

		before(async () => {
			sinon.stub(db, 'findUserByName').callsFake(function (name, cb) {
				return cb(null, {
					rows: [
						{
							active: 1
						}
					]
				});
			});
		});

		after(() => {

		});

		it('FIND should be a function', () => {
			expect(User.find).to.be.a('function');
		});

		it('CHECKHASH should be a function', () => {
			expect(User.checkHash).to.be.a('function');
		});

		it('FIND should resolve with expected payload', done => {
			User.find('username').then(function (data) {
				expect(data).to.be.an('object');
				expect(data.active).to.equal(1);
				done();
			}).catch(function(err){
				console.log(err)
				done();
			});
		});

		it('CHECKHASH should resolve with expected 401 payload', done => {
			User.checkHash('abc123', 'abc123').then(function () {
				done();
			}).catch(function (err) {
				expect(err).to.be.an('object');
				expect(err.status).to.equal(401);
				done();
			});
		});
	});

});